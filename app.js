const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const swagger = require('./swagger/swagger')
const errorHandler = require('./src/middlewares/error-handler')
const cors = require('cors')

const app = express()

app.use(cors())

swagger.startup(app)

const authRouter = require('./src/api/auth/auth-controller')
const tasksRouter = require('./src/api/tasks/tasks-controller')
const usersRouter = require('./src/api/users/users-controller')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/api/v1/auth', authRouter)
app.use('/api/v1/users', usersRouter)
app.use('/api/v1/tasks', tasksRouter)

app.use(errorHandler)

module.exports = app
