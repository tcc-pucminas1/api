class NotFoundError extends Error {
  constructor (message, errors, options = {}) {
    super()
    this.name = 'NotFoundError'
    this.message = message
    this.errorCode = 401
    this.code = 'NotFoundError'
    this.errors = errors
    this.flatten = options.flatten
    this.status = options.status
    this.statusText = options.statusText
    Error.captureStackTrace(this, NotFoundError)
  }
}

module.exports = NotFoundError
