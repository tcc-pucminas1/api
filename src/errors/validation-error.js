class AuthenticationError extends Error {
  constructor (message, errors, options = {}) {
    super()
    this.name = 'ValidationError'
    this.message = message
    this.errorCode = 422
    this.code = 'ValidationError'
    this.errors = errors
    this.flatten = options.flatten
    this.status = options.status
    this.statusText = options.statusText
    Error.captureStackTrace(this, AuthenticationError)
  }
}

module.exports = AuthenticationError
