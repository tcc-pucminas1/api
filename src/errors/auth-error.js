class AuthError extends Error {
  constructor (message, errors, options = {}) {
    super()
    this.name = 'AuthError'
    this.message = message
    this.errorCode = 401
    this.code = 'AuthError'
    this.errors = errors
    this.flatten = options.flatten
    this.status = options.status
    this.statusText = options.statusText
    Error.captureStackTrace(this, AuthError)
  }
}

module.exports = AuthError
