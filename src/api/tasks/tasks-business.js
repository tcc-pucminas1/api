const database = require('../../../database')
const { createPagination, resultPagination } = require('../../utils/pagination')
const NotFoundError = require('../../errors/notfound-error')
const TASK_TABLE = 'tasks'
const USER_TABLE = 'users'
const email = require('../../utils/mail')
const usersBusiness = require('../users/users-business')

const statusConstants = {
  AWAITING: 'AWAITING',
  IN_PROGRESS: 'IN_PROGRESS',
  DONE: 'DONE'
}

const tasksBusiness = (context) => {
  const usersBO = usersBusiness(context)

  const insertTaskOnRepository = (params) => {
    return database(TASK_TABLE)
      .insert(params)
      .returning(['id', 'name', 'description', 'create_user', 'update_user', 'id_status', 'delivery_date', 'created_at', 'updated_at'])
  }

  const updateTaskOnRepository = (taskId, params) => database(TASK_TABLE)
    .update(params)
    .where({ id: taskId })
    .returning(['id', 'name', 'description', 'create_user', 'update_user', 'id_status', 'delivery_date', 'created_at', 'updated_at'])

  const deleteTaskOnRepository = (userId) => database(TASK_TABLE).delete().where({ id: userId })
    .then((taskDeleted) => {
      if (!taskDeleted) {
        throw new NotFoundError('Task não encontrada.')
      }
      return taskDeleted
    })

  const getTasksOnRepository = (pagination, searchParams) => {
    let selectQuery = database
      .column(database.raw("u.first_name || ' ' || u.last_name as responsible_user_name"), database.raw("u2.first_name || ' ' || u2.last_name as create_user_name"))
      .select(`${TASK_TABLE}.*`).from(TASK_TABLE)
      .leftJoin(`${USER_TABLE} as u`, 'u.id', `${TASK_TABLE}.responsible_user_id`)
      .leftJoin(`${USER_TABLE} as u2`, 'u2.id', `${TASK_TABLE}.create_user`)

    if (searchParams.description) {
      selectQuery = selectQuery.where(function () {
        this.where('description', 'ilike', `%${searchParams.description}%`).orWhere('name', 'like', `%${searchParams.description}%`)
      })
    }

    if (searchParams.name) {
      selectQuery = selectQuery.where('name', 'like', `%${searchParams.name}%`)
    }

    if (searchParams.create_user && searchParams.create_user.length) {
      selectQuery = selectQuery.whereIn('create_user', searchParams.create_user)
    }

    if (searchParams.responsible_user_id && searchParams.responsible_user_id.length) {
      selectQuery = selectQuery.whereIn('responsible_user_id', searchParams.responsible_user_id)
    }

    if (searchParams.create_date && searchParams.create_date.length) {
      selectQuery = selectQuery.where(database.raw('tasks.created_at AT TIME ZONE \'America/Sao_Paulo\''), '>=', database.raw(`'${searchParams.create_date[0]}' AT TIME ZONE 'America/Sao_Paulo'`))
      selectQuery = selectQuery.where(database.raw('tasks.created_at AT TIME ZONE \'America/Sao_Paulo\''), '<=', database.raw(`'${searchParams.create_date[1]}' AT TIME ZONE 'America/Sao_Paulo'`))
    }

    if (!pagination) {
      return selectQuery
    }

    return selectQuery.limit(pagination.limit).offset(pagination.offset)
      .then((result) => {
        return result
      })
      .catch((err) => {
        throw err
      })
  }

  const getTaskByIdOnRepository = (taskId) => database.select().from(TASK_TABLE).where('id', taskId).first()
    .then((task) => {
      if (!task) {
        throw new NotFoundError('Task não encontrada')
      }
      return task
    })

  const getTaskDashboardOnRepository = () => {
    return database().select().column(
      database.raw(
        'count(*) filter ( where id_status = \'AWAITING\' )::int as awaiting_status,\n' +
            'count(*) filter ( where id_status = \'IN_PROGRESS\' )::int as in_progress_status,\n' +
            'count(*) filter ( where id_status = \'DONE\' )::int as done_status,\n' +
            'count(*)::int as total_tasks'
      )
    ).from(TASK_TABLE).first()
  }

  const createTask = (params) => {
    const { userId } = context.loggedUser
    const newParams = { ...params, create_user: userId, id_status: statusConstants.AWAITING }
    delete newParams.user_id

    return insertTaskOnRepository(newParams)
      .then(([taskCreated]) => {
        return usersBO.getUserByIdOnRepository(params.responsible_user_id)
          .then((user) => {
            if (params.responsible_user_id) {
              sendEmailWhenUserDefinedOnTask(user.email, params.name)
            }
            return taskCreated
          })
      })
  }

  const getTasks = (params, filters) => {
    const { page, page_size: pageSize } = params
    delete params.page
    delete params.page_size

    const pagination = createPagination(page, pageSize)
    return getTasksOnRepository(pagination, filters)
      .then((tasks) => resultPagination(tasks, page))
  }

  const getTask = (params) => {
    const { id: taskId } = params
    return getTaskByIdOnRepository(taskId)
      .then((tasks) => tasks)
  }

  const updateTask = (params) => {
    const { userId } = context.loggedUser
    const { id: taskId } = params
    const newParams = { ...params, update_user: userId }
    delete newParams.id

    return getTaskByIdOnRepository(taskId)
      .then((taskData) => {
        return updateTaskOnRepository(taskId, newParams)
          .then(([userUpdated]) => {
            return usersBO.getUserByIdOnRepository(params.responsible_user_id)
              .then((user) => {
                if (params.responsible_user_id && params.responsible_user_id !== taskData.responsible_user_id) {
                  sendEmailWhenUserDefinedOnTask(user.email, params.name)
                }
                return userUpdated
              })
          })
      })
  }

  const deleteTask = (params) => {
    const { id: taskId } = params

    return deleteTaskOnRepository(taskId)
  }

  const getTaskDashboard = () => getTaskDashboardOnRepository()

  const sendEmailWhenUserDefinedOnTask = (emailUser, taskTitle) => {
    const subject = 'Você foi adicionado como responsável em uma tarefa'
    const message = `A tarefa com titulo: "${taskTitle}", foi associada ao seu usuário.`
    return email.sendEmail(emailUser, subject, message)
      .catch((err) => {
        console.log('email error', err)
        return true
      })
  }

  return {
    createTask,
    getTasks,
    getTask,
    updateTask,
    deleteTask,
    getTaskDashboard
  }
}

module.exports = tasksBusiness
