const router = require('express').Router()
const taskValidation = require('./tasks-validation')
const { validate } = require('../../middlewares/request-validate')
const { auth } = require('../../middlewares/auth')
const tasksBusiness = require('./tasks-business')
const httpStatus = require('http-status')

/**
 * @swagger
 *
 * components:
 *   schemas:
 *     BaseTask:
 *       type: object
 *       required:
 *         - name
 *         - description
 *         - create_user
 *         - update_user
 *         - id_status
 *         - delivery_date
 *       properties:
 *         name:
 *           type: string
 *           example: taskName
 *         description:
 *           type: string
 *           example: taskDescription
 *         responsible_user_id:
 *           type: integer
 *           example: 1
 *         create_user:
 *           type: integer
 *           example: 1
 *         update_user:
 *           type: integer
 *           example: 1
 *         id_status:
 *           type: integer
 *           example: 1
 *         delivery_date:
 *           type: date-time
 *           example:  2017-07-21
 *
 *     TaskResponse:
 *       schema:
 *       allOf:
 *         - $ref: '#/components/schemas/BaseTask'
 *         - type: object
 *           required:
 *             - id
 *             - active
 *           properties:
 *             id:
 *               type: integer
 *               example: 1
 *             active:
 *               type: boolean
 *               example: true
 *             created_at:
 *               type: string
 *               format: date-time
 *               example: 2019-08-16T00:00:00.000Z
 */

/**
 * @swagger
 *
 * /tasks/{id}:
 *   get:
 *     tags: ['tasks']
 *     summary: Returns task info
 *     parameters:
 *       - name: id
 *         description: task id
 *         in: path
 *         required: true
 *         schema:
 *           type: integer
 *         example: 1
 *     responses:
 *       200:
 *         description: no content
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/TaskResponse'
 */
const getTask = (request, response, next) => {
  return tasksBusiness(request.context)
    .getTask(request.params)
    .then((user) => response.json(user))
    .catch(next)
}

/**
 * @swagger
 *
 * /tasks:
 *   get:
 *     tags: ['tasks']
 *     summary: Returns tasks list
 *     parameters:
 *       - in: query
 *         name: page
 *         description: page number to search for. The default page is 1.
 *         required: false
 *         schema:
 *           type: integer
 *           example: 1
 *       - in: query
 *         name: page_size
 *         description: number of records in a page. The default size is 50.
 *         required: false
 *         schema:
 *           type: integer
 *           maximum: 100
 *           example: 50
 *       - in: query
 *         name: responsible_user_id
 *         description: User responsible for the task
 *         required: false
 *         schema:
 *           type: integer
 *           example: 1
 *       - in: query
 *         name: create_user
 *         description: User who created the task
 *         required: false
 *         schema:
 *           type: integer
 *           example: 1
 *       - in: query
 *         name: description
 *         description: Search for title or description of task
 *         required: false
 *         schema:
 *           type: string
 *           example: perform document adjustment
 *       - in: query
 *         name: id_status
 *         description: Current status of the task
 *         required: false
 *         schema:
 *           type: string
 *           enum: [AWAITING, IN_PROGRESS, DONE]
 *           example: perform document adjustment
 *     responses:
 *       200:
 *         description: no content
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/TaskResponse'
 */
const getTasks = (request, response, next) => {
  const { filters } = request.query
  const filtersObj = filters ? JSON.parse(filters) : {}
  return tasksBusiness(request.context)
    .getTasks(request.query, filtersObj)
    .then((tasks) => response.json(tasks))
    .catch(next)
}

/**
 * @swagger
 *
 * /tasks:
 *   post:
 *     tags: ['tasks']
 *     summary: Register a task
 *     requestBody:
 *       description: Seller data
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - name
 *               - description
 *               - responsible_user_id
 *             properties:
 *               name:
 *                 type: string
 *                 example: send e-mail
 *               description:
 *                 type: string
 *                 example: develop new e-mail functionality
 *               responsible_user_id:
 *                 type: integer
 *                 example: 1
 *     responses:
 *       201:
 *         description: 'successful operation'
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/TaskResponse'
 */
const createTask = (request, response, next) => {
  return tasksBusiness(request.context)
    .createTask(request.body)
    .then((taskCreated) => response.status(httpStatus.CREATED).json(taskCreated))
    .catch(next)
}

/**
 * @swagger
 *
 * /tasks/{id}:
 *   put:
 *     tags: ['tasks']
 *     summary: Update an task
 *     parameters:
 *       - name: id
 *         description: task id
 *         in: path
 *         required: true
 *         schema:
 *           type: integer
 *         example: 1
 *     requestBody:
 *       description: Seller data
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *                 example: send e-mail with template
 *               description:
 *                 type: string
 *                 example: develop new e-mail functionality using template html
 *               id_status:
 *                 type: string
 *                 enum: [AWAITING, IN_PROGRESS, DONE]
 *                 example: AWAITING
 *               responsible_user_id:
 *                 type: number
 *                 example: 1
 *     responses:
 *       200:
 *         description: 'successful operation'
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/TaskResponse'
 */
const updateTask = (request, response, next) => {
  return tasksBusiness(request.context)
    .updateTask({ ...request.params, ...request.body })
    .then((taskUpdated) => response.json(taskUpdated))
    .catch(next)
}

/**
 * @swagger
 *
 * /tasks/{id}:
 *   delete:
 *     tags: ['tasks']
 *     summary: Update an task
 *     parameters:
 *       - name: id
 *         description: task id
 *         in: path
 *         required: true
 *         schema:
 *           type: integer
 *         example: 1
 *     responses:
 *       204:
 *         description: 'successful operation'
 */
const deleteTask = (request, response, next) => {
  return tasksBusiness(request.context)
    .deleteTask(request.params)
    .then(() => response.sendStatus(httpStatus.NO_CONTENT))
    .catch(next)
}

/**
 * @swagger
 *
 * /tasks/dashboard:
 *   get:
 *     tags: ['tasks']
 *     summary: Get data for the dashboard
 *     responses:
 *       201:
 *         description: 'successful operation'
 *         content:
 *           application/json:
 *             type: object
 *             required:
 *               - awaiting_status
 *               - in_progress_status
 *               - done_status
 *               - total
 *             properties:
 *               awaiting_status:
 *                 type: number
 *                 example: 10
 *               in_progress_status:
 *                 type: number
 *                 example: 20
 *               done_status:
 *                 type: number
 *                 example: 35
 *               total:
 *                 type: number
 *                 example: 65
 */
const getTaskDashboard = (request, response, next) => {
  return tasksBusiness(request.context)
    .getTaskDashboard(request.params)
    .then((dashboardData) => response.json(dashboardData))
    .catch(next)
}

router.get('/dashboard', auth, validate(taskValidation.getTask), getTaskDashboard)
router.get('/:id', auth, validate(taskValidation.getTask), getTask)
router.post('/', auth, validate(taskValidation.createTask), createTask)
router.get('/', auth, validate(taskValidation.getTasks), getTasks)
router.put('/:id', auth, validate(taskValidation.updateTask), updateTask)
router.delete('/:id', auth, validate(taskValidation.deleteTask), deleteTask)

module.exports = router
