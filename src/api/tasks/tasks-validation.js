const yup = require('../../utils/yup')

const createTask = yup.object().shape({
  name: yup.string(),
  description: yup.string(),
  responsible_user_id: yup.number().nullable()
})

const updateTask = yup.object().shape({
  id: yup.string(),
  name: yup.string(),
  description: yup.string(),
  id_status: yup.string().oneOf(['AWAITING', 'IN_PROGRESS', 'DONE']),
  delivery_date: yup.string()
})

const getTask = yup.object().shape({
  id: yup.number()
})

const getTasks = yup.object().shape({
  page: yup.number(),
  page_size: yup.number(),
  responsible_user_id: yup.number().nullable(),
  create_user: yup.number().nullable(),
  description: yup.string().nullable(),
  id_status: yup.string().oneOf(['AWAITING', 'IN_PROGRESS', 'DONE', null]).nullable()
})

const deleteTask = yup.object().shape({
  id: yup.number()
})

module.exports = {
  createTask,
  updateTask,
  getTask,
  getTasks,
  deleteTask
}
