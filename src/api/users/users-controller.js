const router = require('express').Router()
const userValidation = require('./users-validation')
const { validate } = require('../../middlewares/request-validate')
const { auth } = require('../../middlewares/auth')
const usersBusiness = require('./users-business')
const httpStatus = require('http-status')

/**
 * @swagger
 *
 * components:
 *   schemas:
 *     BaseUser:
 *       type: object
 *       required:
 *         - email
 *         - password
 *         - first_name
 *         - last_name
 *         - phone
 *       properties:
 *         email:
 *           type: string
 *           example: user@email.com
 *         password:
 *           type: string
 *           example: "123456789"
 *         first_name:
 *           type: string
 *           example: first
 *         last_name:
 *           type: string
 *           example: last
 *         phone:
 *           type: string
 *           example: "3499999999"
 *
 *     UserResponse:
 *       schema:
 *       allOf:
 *         - $ref: '#/components/schemas/BaseUser'
 *         - type: object
 *           required:
 *             - id
 *             - active
 *           properties:
 *             id:
 *               type: integer
 *               example: 1
 *             active:
 *               type: boolean
 *               example: true
 *             created_at:
 *               type: string
 *               format: date-time
 *               example: 2019-08-16T00:00:00.000Z
 */

/**
 * @swagger
 *
 * /users/{id}:
 *   get:
 *     tags: ['users']
 *     summary: Returns user info
 *     parameters:
 *       - name: id
 *         description: user id
 *         in: path
 *         required: true
 *         schema:
 *           type: integer
 *         example: 1
 *     responses:
 *       200:
 *         description: no content
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/UserResponse'
 */
const getUser = (request, response, next) => {
  return usersBusiness(request.context)
    .getUser(request.params)
    .then((user) => response.json(user))
    .catch(next)
}

/**
 * @swagger
 *
 * /users:
 *   get:
 *     tags: ['users']
 *     summary: Returns users list
 *     parameters:
 *       - in: query
 *         name: page
 *         description: page number to search for. The default page is 1.
 *         required: false
 *         schema:
 *           type: integer
 *           example: 1
 *       - in: query
 *         name: page_size
 *         description: number of records in a page. The default size is 50.
 *         required: false
 *         schema:
 *           type: integer
 *           maximum: 100
 *           example: 50
 *     responses:
 *       200:
 *         description: no content
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/UserResponse'
 */
const getUsers = (request, response, next) => {
  return usersBusiness(request.context)
    .getUsers(request.query)
    .then((users) => response.json(users))
    .catch(next)
}

/**
 * @swagger
 *
 * /users:
 *   post:
 *     tags: ['users']
 *     summary: Register an user
 *     requestBody:
 *       description: Seller data
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/BaseUser'
 *     responses:
 *       201:
 *         description: 'successful operation'
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/UserResponse'
 */
const createUser = (request, response, next) => {
  return usersBusiness(request.context)
    .createUser(request.body)
    .then((userCreated) => response.status(httpStatus.CREATED).json(userCreated))
    .catch(next)
}

/**
 * @swagger
 *
 * /users/{id}:
 *   put:
 *     tags: ['users']
 *     summary: Update an user
 *     parameters:
 *       - name: id
 *         description: user id
 *         in: path
 *         required: true
 *         schema:
 *           type: integer
 *         example: 1
 *     requestBody:
 *       description: Seller data
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - username
 *               - first_name
 *               - last_name
 *               - phone
 *             properties:
 *               username:
 *                 type: string
 *                 example: username
 *               first_name:
 *                 type: string
 *                 example: first
 *               last_name:
 *                 type: string
 *                 example: last
 *               phone:
 *                 type: string
 *                 example: "3499999999"
 *     responses:
 *       200:
 *         description: 'successful operation'
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/UserResponse'
 */
const updateUser = (request, response, next) => {
  return usersBusiness(request.context)
    .updateUser({ ...request.params, ...request.body })
    .then((userUpdated) => response.json(userUpdated))
    .catch(next)
}

/**
 * @swagger
 *
 * /users/{id}:
 *   delete:
 *     tags: ['users']
 *     summary: Delete an user
 *     parameters:
 *       - name: id
 *         description: user id
 *         in: path
 *         required: true
 *         schema:
 *           type: integer
 *         example: 1
 *     responses:
 *       204:
 *         description: 'successful operation'
 */
const deleteUser = (request, response, next) => {
  return usersBusiness(request.context)
    .deleteUser(request.params)
    .then(() => response.sendStatus(httpStatus.NO_CONTENT))
    .catch(next)
}

router.get('/:id', auth, validate(userValidation.getUser), getUser)
router.get('/', auth, validate(userValidation.getUsers), getUsers)
router.post('/', validate(userValidation.createUser), createUser)
router.put('/:id', auth, validate(userValidation.updateUser), updateUser)
router.delete('/:id', validate(userValidation.deleteUser), deleteUser)

module.exports = router
