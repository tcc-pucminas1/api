const yup = require('../../utils/yup')

const createUser = yup.object().shape({
  username: yup.string().max(30),
  email: yup.string(),
  password: yup.string(),
  first_name: yup.string(),
  last_name: yup.string(),
  phone: yup.string()
})

const updateUser = yup.object().shape({
  id: yup.number(),
  first_name: yup.string(),
  last_name: yup.string(),
  phone: yup.string()
})

const getUser = yup.object().shape({
  id: yup.number()
})

const getUsers = yup.object().shape({})

const deleteUser = yup.object().shape({
  id: yup.number()
})

module.exports = {
  createUser,
  updateUser,
  getUser,
  getUsers,
  deleteUser
}
