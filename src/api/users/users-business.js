const database = require('../../../database')
const { generatePassword, generateRandomSalt } = require('../../utils/crypt')
const { createPagination, resultPagination } = require('../../utils/pagination')
const NotFoundError = require('../../errors/notfound-error')
const USER_TABLE = 'users'
const moment = require('moment')

const usersBusiness = (context) => {
  const insertUserOnRepository = (params) => database(USER_TABLE)
    .insert(params)
    .returning(['id', 'email', 'first_name', 'last_name', 'phone', 'created_at'])

  const updateUserOnRepository = (userId, params) => database(USER_TABLE)
    .update({ ...params, updated_at: moment() })
    .where({ id: userId })
    .returning(['id', 'email', 'first_name', 'last_name', 'phone', 'created_at', 'updated_at'])

  const deleteUserOnRepository = (userId) => database(USER_TABLE).delete().where({ id: userId })

  const getUsersOnRepository = (pagination) => {
    if (!pagination) {
      return database.select().from(USER_TABLE)
        .orderBy([{ column: 'created_at', order: 'desc' }])
    }
    return database.select().from(USER_TABLE)
      .orderBy([{ column: 'created_at', order: 'desc' }])
      .limit(pagination.limit).offset(pagination.offset)
  }

  const getUserByIdOnRepository = (userId) => database.select().from(USER_TABLE).where('id', userId).first()

  const getUserByEmailOnRepository = (userEmail) => database.select().from(USER_TABLE).where('email', userEmail)

  const getUsers = ({ page, page_size: pageSize }) => {
    const pagination = createPagination(page, pageSize)
    return getUsersOnRepository(pagination)
      .then((users) => {
        return users.map((user) => {
          const userCreatedDate = moment(user.created_at).utcOffset('-0300').format('DD/MM/YYYY HH:MM:SS')
          const userUpdateDate = user.updated_at ? moment(user.updated_at).utcOffset('-0300').format('DD/MM/YYYY HH:MM:SS') : null
          return { ...user, created_at: userCreatedDate, updated_at: userUpdateDate }
        })
      })
      .then((users) => {
        return resultPagination(users, page)
      })
  }

  const getUserOrFail = (userId) => getUserByIdOnRepository(userId)
    .then((user) => {
      if (!user) {
        throw new NotFoundError('Usuário não encontrado.')
      }
      return user
    })

  const getUser = ({ id }) => getUserOrFail(id)
    .then((users) => users)

  const createUser = (params) => {
    const passwordSalt = generateRandomSalt()
    const passwordUser = generatePassword(params.password, passwordSalt)
    return insertUserOnRepository({ ...params, password: passwordUser, salt: passwordSalt })
      .then(([userCreated]) => userCreated)
  }

  const updateUser = (params) => {
    const { id: userId } = params
    return updateUserOnRepository(userId, params)
      .then(([userUpdated]) => userUpdated)
  }

  const deleteUser = (params) => {
    const { id: userId } = params
    return deleteUserOnRepository(userId)
  }

  return {
    createUser,
    getUsers,
    getUser,
    getUserByEmailOnRepository,
    updateUser,
    deleteUser,
    getUserByIdOnRepository
  }
}

module.exports = usersBusiness
