const database = require('../../../database')
const { generatePassword } = require('../../utils/crypt')
const { generateJwtToken } = require('../../utils/jwt')
const AuthError = require('../../errors/auth-error')
const NotFoundError = require('../../errors/notfound-error')
const USER_TABLE = 'users'

const authBusiness = () => {
  const findUserByEmailOnRepository = (email) => database.select('*')
    .from(USER_TABLE)
    .where('email', email)

  const authenticate = ({ email, password }) => {
    return findUserByEmailOnRepository(email)
      .then(([user]) => {
        if (!user) {
          throw new NotFoundError('User not found.')
        }

        const securePassword = generatePassword(password, user.salt)
        if (user.password !== securePassword) {
          throw new AuthError('User not authorized')
        }

        const payload = { email, username: user.username, first_name: user.first_name, last_name: user.last_name  }
        const authToken = generateJwtToken(payload)

        return {
          access_token: authToken
        }
      })
  }

  return {
    authenticate
  }
}

module.exports = authBusiness
