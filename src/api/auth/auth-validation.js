const yup = require('../../utils/yup')

const authenticate = yup.object().shape({
  email: yup.string(),
  password: yup.string()
})

module.exports = {
  authenticate
}
