const router = require('express').Router()
const authValidation = require('./auth-validation')
const { validate } = require('../../middlewares/request-validate')
const authBusiness = require('./auth-business')

/**
 * @swagger
 *
 * components:
 *   schemas:
 *     AuthResponse:
 *       type: object
 *       required:
 *         - access_token
 *         - expires_in
 *         - refresh_token
 *       properties:
 *         access_token:
 *           type: string
 *           example: '<access_token>'
 *         expires_in:
 *           type: integer
 *           format: int32
 *           example: 28800
 *         refresh_token:
 *           type: string
 *           example: '<refresh_token>'
 */

/**
 * @swagger
 *
 * /auth:
 *   post:
 *     tags: ['auth']
 *     summary: Authentication
 *     requestBody:
 *       required: true
 *       description: Object with params
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             required:
 *               - email
 *               - password
 *             properties:
 *               email:
 *                 type: string
 *                 example: email@email.com.br
 *               password:
 *                 type: string
 *                 example: '123456789'
 *     responses:
 *       200:
 *         description: 'successful operation'
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/AuthResponse'
 *     security: []
 */
const authenticate = (request, response, next) => {
  return authBusiness()
    .authenticate(request.body)
    .then((auth) => response.json(auth))
    .catch(next)
}

router.post('/', validate(authValidation.authenticate), authenticate)

module.exports = router
