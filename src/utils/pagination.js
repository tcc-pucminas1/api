const createPagination = (page = 0, pageSize = 50) => {
  return {
    limit: Number(pageSize),
    offset: (Number(pageSize) * Number(page)) - Number(pageSize)
  }
}

const resultPagination = (records = [], page) => {
  return {
    page: Number(page),
    page_size: records.length,
    records: records
  }
}

module.exports = {
  createPagination,
  resultPagination
}
