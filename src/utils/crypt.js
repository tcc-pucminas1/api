const crypto = require('crypto')
const jwt = require('jsonwebtoken')

const generateRandomSalt = () => crypto.randomBytes(32).toString('hex')

const generatePassword = (password, salt) => crypto.pbkdf2Sync(password, salt, 10000, 64, 'sha512').toString('hex')

const secret = process.env.API_JWT_SECRET

const generateJwtToken = (email) => jwt.sign(
  { email: email },
  secret,
  {
    expiresIn: '2h' // expires in 24 hours
  }
)

module.exports = {
  generatePassword,
  generateRandomSalt,
  generateJwtToken
}
