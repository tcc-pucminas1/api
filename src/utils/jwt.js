const jwt = require('jsonwebtoken')

const secret = process.env.API_JWT_SECRET

const generateJwtToken = (payload) => jwt.sign(
  payload,
  secret,
  {
    expiresIn: '12h' // expires in 24 hours
  }
)

module.exports = {
  jwt,
  generateJwtToken
}
