const nodemailer = require('nodemailer')

const mail = () => {
  const emailUser = 'pucminas2020@gmail.com'
  const emailPassword = 'Puc2020minas'

  // create reusable transporter object using the default SMTP transport
  const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: emailUser, // generated ethereal user
      pass: emailPassword // generated ethereal password
    }
  })

  const sendEmail = (emailTo, subject, emailBody) => {
    return transporter.sendMail({
      from: `"iTask - Sistema de controle de atividades" <${emailUser}>`,
      to: emailTo,
      subject: subject,
      text: emailBody
    })
  }

  return {
    sendEmail
  }
}

module.exports = mail()
