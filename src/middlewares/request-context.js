const uuid = require('uuid')

module.exports = (user = {}, token = null) => Object.freeze({
  reqId: uuid.v4(),
  loggedUser: {
    userId: user.id,
    token
  }
})
