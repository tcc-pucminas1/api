const { jwt } = require('../utils/jwt')
const AuthError = require('../errors/auth-error')
const usersBO = require('../api/users/users-business')()
const requestContext = require('./request-context')

const jwtVerify = (token, secret) => new Promise((resolve, reject) => {
  jwt.verify(token, secret, (err, decoded) => {
    if (err) {
      reject(err)
    } else {
      resolve(decoded)
    }
  })
})

const auth = (request, response, next) => {
  const { authorization } = request.headers
  if (!authorization) {
    throw new AuthError('Error to auth')
  }

  const token = authorization.slice(7)
  return jwtVerify(token, process.env.API_JWT_SECRET)
    .then((jwtDecoded) => {
      return usersBO.getUserByEmailOnRepository(jwtDecoded.email)
        .then(([user]) => {
          request.context = requestContext(user, token)
          return next()
        })
    })
    .catch((err) => {
      throw err
    })
}

module.exports = {
  auth
}
