const errorHandler = (err, req, res, next) => {
  if (res.headersSent) {
    return next(err)
  }

  const errorCode = err.errorCode || 500
  res.status(err.errorCode || 500).json({ error_code: errorCode, message: err.message, errors: err.errors || []  })
}

module.exports = errorHandler
