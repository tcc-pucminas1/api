const ValidationError = require('../errors/validation-error')

const validate = (schema) => {
  return (request, response, next) => {
    const params = { ...request.params, ...request.body, ...request.query }

    try {
      schema.validateSync(params, { abortEarly: false, strict: false })
      return next()
    } catch (error) {
      return next(new ValidationError('validationError', error.errors))
    }
  }
}

module.exports = {
  validate
}
