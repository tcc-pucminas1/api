exports.up = function (knex) {
  return knex.schema
    .createTable('users', function (table) {
      table.increments('id')
      table.string('email', 255).unique().notNullable()
      table.string('password', 255).notNullable()
      table.string('salt', 255).notNullable()
      table.string('first_name', 255).notNullable()
      table.string('last_name', 255).notNullable()
      table.string('phone', 11).notNullable()
      table.string('active').defaultTo(true).notNullable()
      table.dateTime('created_at').defaultTo(knex.fn.now()).notNullable()
      table.dateTime('updated_at')
    })
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('users')
}
