exports.up = function (knex) {
  return knex.schema
    .createTable('tasks', function (table) {
      table.increments('id')
      table.string('name', 255).notNullable()
      table.string('description', 255).notNullable()
      table.string('id_status').unsigned().notNullable()
      table.foreign('id_status').references('id').inTable('status')
      table.dateTime('delivery_date')
      table.integer('responsible_user_id')
      table.foreign('responsible_user_id').references('id').inTable('users')
      table.integer('update_user')
      table.foreign('update_user').references('id').inTable('users')
      table.integer('create_user')
      table.foreign('create_user').references('id').inTable('users')
      table.dateTime('created_at').defaultTo(knex.fn.now()).notNullable()
      table.dateTime('updated_at')
    })
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('tasks')
}
