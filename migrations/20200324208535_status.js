exports.up = function (knex) {
  return knex.schema.createTable('status', function (table) {
    table.string('id', 255).unique().notNullable()
  }).then(function () {
    return knex('status').insert([
      { id: 'AWAITING' },
      { id: 'IN_PROGRESS' },
      { id: 'DONE' }
    ])
  })
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('task_status')
}
