const swaggerJSDoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')
const swaggerOpts = require('./swagger-config')

const swaggerSpec = swaggerJSDoc(swaggerOpts)
const options = {
  // Hide the models section
  customCss: '.swagger-ui section.models { display: none }'
}

const startup = (app) => {
  app.use('/api/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec, options))
  app.use('/api/docs.json', (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    res.send(swaggerSpec)
  })
}

module.exports = {
  swaggerSpec,
  startup
}
