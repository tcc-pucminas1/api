module.exports = {
  swaggerDefinition: {
    openapi: '3.0.0',
    info: {
      title: 'iTask',
      version: 'dev',
      description: 'APIs'
    },
    servers: [{
      url: '/api/v1/',
      description: 'iTask Api'
    }],
    components: {
      securitySchemes: {
        bearerToken: {
          type: 'http',
          scheme: 'bearer',
          bearerFormat: 'JWT'
        }
      }
    },
    security: [{
      bearerToken: []
    }]
  },
  apis: ['./src/api/**/*-controller.js']
}
