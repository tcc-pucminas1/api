require('dotenv').config()

module.exports = {
  development: {
    client: 'postgresql',
    connection: {
      host: process.env.API_DB_HOST,
      database: process.env.API_DB_NAME,
      user: process.env.API_DB_USER,
      password: process.env.API_DB_PASSWORD
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'migrations'
    }
  },

  production: {
    client: 'postgresql',
    connection: {
      host: process.env.API_DB_HOST,
      database: process.env.API_DB_NAME,
      user: process.env.API_DB_USER,
      password: process.env.API_DB_PASSWORD
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'migrations'
    }
  }

}
